package datreon;

import java.sql.Connection;

/**
 * Created by datreon on 6/8/17.
 */
public class JDBC_MAIN {

    public static void main(String[] args) {

        //ConnectionJdbc connectionJdbc = new ConnectionJdbc();


        Connection connection = null;
        CRUD_JDBC crudJdbc = new CRUD_JDBC(connection);

//        crudJdbc.setJdbcurl("jdbc:mysql://192.168.100.114:{port}/DB_name");
//        crudJdbc.setUsername("root");
//        crudJdbc.getUsername();
//        crudJdbc.setPassword("Qwerty@321");

        crudJdbc.enterConnectionDetails();
        crudJdbc.makeConnection();

        crudJdbc.setCreateTableSQL("CREATE TABLE  Users"+
                "(" +
                "    id INT(11), " +
                "    name VARCHAR(255)," +
                "    age INT(11)," +
                "    email VARCHAR(255)" +
                ")" );
        crudJdbc.getCreateTableSQL();

        crudJdbc.createTable();

        crudJdbc.insertRow(3,"Joy",19,"abc@gmail.com");

        crudJdbc.readTable();

        crudJdbc.updateRow(3, 20);

        crudJdbc.readTable();

    }
}